const ones = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
const teens = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
const tens = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

function numberToWords(number) {
  const digits = number.toString().split('').map(string => parseInt(string)).reverse() // turns 1234 to [4, 3, 2, 1]
  let result = ''
  for (let i = 0; i < digits.length; i++) {
    const digit = digits[i]

    if (i === 0)                     result = ones[digit]
    else if (i === 1 && digit === 1) result = teens[digits[i - 1]]
    else if (i === 1)                result = tens[digit] + ' and ' + result
    else if (i === 2 && digit > 0)   result = ones[digit] + ' hundred ' + result
    else if (i === 3 && digit > 0)   result = ones[digit] + ' thousand ' + result
  }
  return result
}

function countLetters(min, max) {
  let result = 0
  for (let i = min; i <= max; i++) {
    result += numberToWords(i).toString().replace(/\s/g, '').length
  }
  return result
}

console.log(countLetters(1, 1000))