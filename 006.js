const createArray = max => {
	let arr = []
	for (let i = 1; i <= max; i++) {
		arr.push(i)
	}
	return arr
}

const sumOfSquares = max => {
	return createArray(max).reduce((total, num) => {
		return total += Math.pow(num, 2)
	})
}

const squareOfSums = max => {
	return Math.pow(createArray(max).reduce((total, num) => {
		return total += num
	}), 2)
}

console.log(squareOfSums(100) - sumOfSquares(100))