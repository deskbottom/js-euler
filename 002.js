let numbers = [0, 1]
let result = 0

while (numbers[1] < 4000000) {
	if (numbers[1] % 2 === 0) result += numbers[1]
	numbers = [numbers[1], numbers[0] + numbers[1]]
}

console.log(result)