const fs = require('fs')

/**
 * Reads text file, parses it into an array of 10-digit numbers, then runs largeSum.
 */
fs.readFile('./013.txt', 'utf8', (err, data) => {
	if (err) throw err;
	const numbers = data.split('\n').map(text => parseInt(text))
  console.log(largeSum(numbers))
});

/**
 * Adds a list of numbers together and returns its first 10 digits.
 */
function largeSum(numbers) {
	total = 0
	numbers.forEach(number => {
		total += number
	})
	return fullNotation(total).slice(0, 10)
}
	
/**
 * Converts a number into its full notation.
 */
function fullNotation(num) {
	let str = ''
	do {
		let a = num % 10
		num = Math.trunc(num / 10)
		str = a + str
	} while (num > 0)
	return str
}