function palindrome(num) {
	const letters = num.toString().split('')
	for(let i = 0; i < Math.floor(letters.length/2); i++) {
		if (letters[i].localeCompare(letters[letters.length - (i + 1)]) !== 0) return false;
	}
	return true;
}

let largestPalindrome = 0;
for (let firstNum = 100; firstNum < 1000; firstNum++) {
	for (let secondNum = 100; secondNum < 1000; secondNum++) {
		if(palindrome(firstNum * secondNum) && firstNum * secondNum > largestPalindrome) largestPalindrome = firstNum * secondNum
	}
}
console.log(largestPalindrome)