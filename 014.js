function longestCollatzSequence(max) {
	let longestSequence = 0
	let largestLength = 0
	for (let i = 1; i <= max; i++) {
		const length = sequenceLength(i)
		if (length > largestLength) {
			longestSequence = i
			largestLength = length
		}
	}
	return longestSequence
}

function sequenceLength(num) {
	if (num <= 1) return [num]
	let result = 0

	while (num > 1) {
		result++
		if (num % 2 === 0) num /= 2
		else num = num * 3 + 1
	}
	return result + 1
}


console.log(longestCollatzSequence(1000000))