/**
 * Determines if a given number is prime.
 */
function prime(num) {
	if (num < 2 || !Number.isInteger(num)) return false
	if (num <= 3) return true
	if (num % 2 === 0 || num % 3 === 0) return false

	let i = 5
	let w = 2

	while (i * i <= num) {
		if (num % i === 0) return false
		i += w
		w = 6 - w
	}

	return true
}

/**
 * Calculates the prime factors of a number.
 */
function primeFactors(num, currentList = []) {
	if (prime(num) || num === 1) return currentList.concat(num)

	let i = 2
	while (true) {
		if (num % i === 0 && prime(i)) return primeFactors(num / i, currentList.concat(i))
		i++
	}
}

/**
 * Counts the number of occurences of each number in a set of numbers.
 * @returns {object} Keys are the number, values are the occurences of that number.
 */
function countOccurences(numbers) {
	let result = {}
	for (let i = 0; i < numbers.length; i++) {
		const prime = numbers[i]
		if (!(prime in result)) result[prime.toString()] = 1
		else result[prime.toString()]++
	}
	return result
}

/**
 * Counts the number of divisors of a number.
 */
function numberOfDivisors(num) {
	const primes = primeFactors(num)
	const primeCounts = Object.values(countOccurences(primes))

	let result = 1
	primeCounts.forEach(count => {
		result *= count + 1
	})
	
	return result
}

/**
 * Counts the number of divisors in each triangle number until the number exceeds 500.
 * @returns {number} Triangle number whose divisor count exceeds 500.
 */
function checkTriangles(current, increment) {
	while (true) {
		if (numberOfDivisors(current) > 500) return current
		else {
			current += increment
			increment++
		}
	}
	
}

console.log(checkTriangles(1, 2))