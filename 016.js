class Num {
  constructor(value) {
    if (typeof value === 'number') this.digits = value.toString().split('').map(string => parseInt(string))
    else if(typeof value === 'object') this.digits = value
  }

  jsValue() {
    return parseInt(this.toString())
  }

  toString() {
    return this.digits.join('')
  }

  /**
   * Adds 0s to the left of the current value. Used for addition.
   */
  fillPlaces(num) {
    let result = this.digits
    while (this.digits.length < num) {
      result.unshift(0)
    }
    return result
  }

  pow(num) {
    let result = new Num(this.jsValue())
    for (let i = 0; i < num.jsValue() - 1; i++) {
      result = result.times(this)
    }
    return result
  }
  
  times(num) {
    let result = new Num(0)
    for (let i = 0; i < num.jsValue(); i++) {
      result = result.plus(this)
    }
    return result
  }

  plus(number) {
    let result = []
    let digits = this.fillPlaces(number.digits.length)
    let digitsToAdd = number.fillPlaces(this.digits.length)
    for (let i = this.digits.length - 1; i >= 0; i--) {
      let val = digits[i] + digitsToAdd[i]
      if (val >= 10) {
        result.unshift(val - 10)
        if (i > 0) digits[i - 1] += 1
        else result.unshift(1)
      }
      else result.unshift(val)
    }
    return new Num(result)
  }

  sumOfDigits() {
    let result = 0
    this.digits.forEach(num => {
      result += num
    })
    return result
  }
}

console.log(new Num(2).pow(new Num(1000)).sumOfDigits())