function pythagoreanTriplet(a, b, c) {
	return Math.pow(a, 2) + Math.pow(b, 2) === Math.pow(c, 2)
}

for (let a = 1; a < 1000; a++) {
	for (let b = a; b < 1000; b++) {
		for (let c = b; c < 1000; c++) {
			if (a + b + c === 1000 && pythagoreanTriplet(a, b, c)) console.log(a * b * c)
		}
	}
}