/**
 * Creates an array to represent Pascal's triangle up to a given number of rows.
 */
function pascalTriangle(size) {
	let arr = [[1], [1, 1]]
	while (arr.length < size) {
		arr.push([1])
		const newRow = arr[arr.length - 1]
		const prevRow = arr[arr.length - 2]
		for (let i = 0; i < prevRow.length - 1; i++) {
			newRow.push(prevRow[i] + prevRow[i + 1])
		}
		newRow.push(1)
	}
	return arr
}

/**
 * Finds the maximum number of paths from the start to end of a square of a given size (only moving south and east).
 */
function latticePaths(size) {
	const triangle = pascalTriangle(size * 2 + 1)
	const lastRow = triangle[triangle.length - 1]
	return lastRow[Math.floor(lastRow.length / 2)]
}

console.log(latticePaths(20))