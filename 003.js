function prime(num) {
	if (num < 2 || !Number.isInteger(num)) return false
	if (num <= 3) return true
	if (num % 2 === 0 || num % 3 === 0) return false

	let i = 5
	let w = 2

	while (i * i <= num) {
		if (num % i === 0) return false
		i += w
		w = 6 - w
	}

	return true
}

function largestPrimeFactor(original) {
	let num = 1

	while (true) {
		num += 1
		if (prime(original/num)) return original/num
	}
}

console.log(largestPrimeFactor(600851475143))