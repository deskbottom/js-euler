/**
 * Determines if a given number is prime.
 */
function prime(num) {
	if (num < 2 || !Number.isInteger(num)) return false
	if (num <= 3) return true
	if (num % 2 === 0 || num % 3 === 0) return false

	let i = 5
	let w = 2

	while (i * i <= num) {
		if (num % i === 0) return false
		i += w
		w = 6 - w
	}

	return true
}


function largestPrime(max) {
	let primesFound = 2
	let i = 3

	while (primesFound < max) {
		i += 2
		if(prime(i)) primesFound++
	}

	return i
}


console.log(largestPrime(10001))