/**
 * Determines if a given number is prime.
 */
function prime(num) {
	if (num < 2 || !Number.isInteger(num)) return false
	if (num <= 3) return true
	if (num % 2 === 0 || num % 3 === 0) return false

	let i = 5
	let w = 2

	while (i * i <= num) {
		if (num % i === 0) return false
		i += w
		w = 6 - w
	}

	return true
}

/**
 * Calculates the prime factors of a number.
 */
function primeFactors(num, currentList = []) {
	if (prime(num) || num === 1) return currentList.concat(num)

	let i = 2
	while (true) {
		if (num % i === 0 && prime(i)) return primeFactors(num / i, currentList.concat(i))
		i++
	}
}

/**
 * Creates an object to record prime factors and their highest number of occurences in an individual set.
 * @param {Array} primeSets - list of prime factor sets.
 */
function filterPrimes(primeSets) {
	const result = {}

	primeSets.forEach(set => {
		let primes = {}
		set.forEach(prime => {
			if (prime.toString() in primes) primes[prime.toString()]++
			else primes[prime.toString()] = 1
		})

		for (let prime in primes) {
			if (!(prime in result)) result[prime] = primes[prime]
			else if (result[prime] < primes[prime]) result[prime] = primes[prime]
		}
	})

	return result
}

function lcm(nums) {
	let factorSets = []
	nums.forEach(num => {
		factorSets.push(primeFactors(num))
	})

	const filtered = filterPrimes(factorSets)

	let result = 1
	for (let prime in filterPrimes(factorSets)) {
		result *= Math.pow(parseInt(prime), filtered[prime])
	}
	return result
}

console.log(lcm([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]))